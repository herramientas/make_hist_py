#!/usr/bin/env python3

###############################################################################
# Este programa realiza un histograma a partir de un archivo. Vea la ayuda    #
# incorporada para mas informacion.                                           #
#                                                                             #
# Ejemplo de uso:                                                             #
#     ./make_hist.py archivo.dat                                              #
# Eso imprime la Transformada en archivo_hist.dat                             #
#                                                                             #
# Este programa queda en dominio publico. Puede modificarse y relicenciarse.  #
#                                                                             #
# Autor: Victor Hugo Purrello                                    Version: 0.1 #
###############################################################################

import numpy as np
import os
import argparse

parser = argparse.ArgumentParser (
        description='Make histogram from file.',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument ('file',
        help='Input file')
parser.add_argument ('-b',
        default=200,
        type=int,
        help='Amount of bins (use 0 for "auto" mode)')
parser.add_argument ('-c',
        default=1,
        type=int,
        help='Column to process (numbered from 1)')
parser.add_argument ('-n',
        help='Make normalized histogram',
        action="store_true")
parser.add_argument ('-s',
        default=0,
        type=float,
        help='Size of bins (prevails over amount of bins)')
parser.add_argument ('-t',
        default="",
        type=str,
        help='String to search in the first column')
parser.add_argument ('-v',
        help='Be verbose',
        action="store_true")
args = parser.parse_args()

FULLFILENAME = args.file
FILENAME = os.path.split(FULLFILENAME)[1]
BINS = args.b
COL = args.c
NORM = args.n
TAG = args.t
SIZE = args.s
VERBOSE = args.v

if VERBOSE:
    print ("Processing file %s" %FULLFILENAME)

if not os.path.isfile(FULLFILENAME):
    print ("%s does not exists" %FULLFILENAME)
    exit(1)

fopen = open

line = ''

OMODE = "w"
if TAG == "":
    OUTNAME = FILENAME[:-4]+"_hist_c_%d.dat" %COL
else:
    OUTNAME = FILENAME[:-4]+"_t_"+TAG+"_hist_c_%d.dat" %COL
if os.path.isfile(OUTNAME):
    print ("Overwriting %s" %OUTNAME)

with fopen(FULLFILENAME, 'r') as FILE:
    data = []
    for line in FILE:
        if line == '\n':
            continue
        elif line == '':
            continue
        elif line[0] == '#':
            continue
        if TAG != "":
            if line[:len(TAG)] != TAG:
                continue
        data.append(line.split()[COL-1])

    data = np.array(data, dtype=float)

    if BINS == 0:
        BINS = "auto"

    if SIZE != 0:
        BINS = int ((data.max() - data.min()) / SIZE)
        if BINS < 2:
            print ("Not enough bins to make histogram")
            exit(1)

    hist_y,hist_x = np.histogram(data,bins=BINS,density=NORM)

# density : bool, optional
# If False, the result will contain the number of samples in each bin.
# If True, the result is the value of the probability density function
# at the bin, normalized such that the integral over the range is 1.
# Note that the sum of the histogram values will not be equal to 1 unless
# bins of unity width are chosen; it is not a probability mass function.

    with open(OUTNAME,mode=OMODE) as OUTFILE:
        for j in range(len(hist_y)):
            OUTFILE.write("%e %e\n" %((hist_x[j]+hist_x[j+1])/2, hist_y[j]))

