Este programa realiza un histograma a partir de un archivo. Vea la ayuda
incorporada para mas informacion.

Ejemplo de uso:
    ./make_hist.py archivo.dat

Eso imprime la Transformada en archivo_hist.dat

Este programa queda en dominio publico. Puede modificarse y relicenciarse.
